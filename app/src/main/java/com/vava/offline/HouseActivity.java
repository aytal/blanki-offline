package com.vava.offline;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class HouseActivity extends AppCompatActivity {

    EditText vidOtherET, kadastrET, addressET;

    RadioGroup procedureRG, typeOfPropertyRG;

    AlertDialog.Builder confirmationDialog;
    SmsManager sm;
    ArrayList<String> parts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house);

        confirmationDialog = new AlertDialog.Builder(this);

        procedureRG = findViewById(R.id.procedure);
        typeOfPropertyRG = findViewById(R.id.typeOfProperty);
        vidOtherET = findViewById(R.id.vidOther);
        kadastrET = findViewById(R.id.kadastr);
        addressET = findViewById(R.id.address);

    }

    public void onSendButtonClick(View view) {

        String data = "2";
        data += "#";
        if (procedureRG.getCheckedRadioButtonId() == R.id.uchetRega) {
            data += "учет и регистрация";
        } else if (procedureRG.getCheckedRadioButtonId() == R.id.uchet) {
            data += "учет";
        } else {
            data += "регистрация";
        }

        data += "#";

        if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.zemRadioButton) {
            data += "Земельный участок";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.buildingRadioButton) {
            data += "Здание";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.complexRadioButton) {
            data += "Единый недвижимый комплекс";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.constructionRadioButton) {
            data += "Сооружение";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.unfinishedRadioButton) {
            data += "Объект незавершенного строительства";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.enterpriseRadioButton) {
            data += "Предприятие как имущественный комплекс";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.roomRadioButton) {
            data += "Помещение";
        } else if (typeOfPropertyRG.getCheckedRadioButtonId() == R.id.machineRadioButton) {
            data += "Машино-место";
        } else {
            data += vidOtherET.getText().toString();
        }

        data += "#";
        data += kadastrET.getText().toString();
        data += "#";

        if (addressET.getText().toString().equals(""))
            data += " ";
        else
            data += addressET.getText().toString();

        data = data.replaceAll("И", "и");

        sendSMS(data);
    }

    private void sendSMS(String message) {
        sm = SmsManager.getDefault();
        parts = sm.divideMessage(message);

        confirmationDialog.setMessage(  "Ваше заявление отправится с помощью " + parts.size() +
                " SMS. Вы уверены, что хотите подать заявление?");
        confirmationDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sm.sendMultipartTextMessage(getString(R.string.server_phone), null, parts, null, null);
                Toast.makeText(HouseActivity.this, "Ваше заявление подано.", Toast.LENGTH_LONG).show();
            }
        });

        confirmationDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        confirmationDialog.show();
    }
}