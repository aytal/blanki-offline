package com.vava.offline;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class IpActivity extends AppCompatActivity {

    EditText    secondNameET, firstNameET, patronusET,
                innET, birthDateET, birthPlaceET;
    RadioGroup sexRG, citizenshipRG;

    AlertDialog.Builder confirmationDialog;
    SmsManager sm;
    ArrayList<String> parts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ip);

        confirmationDialog = new AlertDialog.Builder(this);

        secondNameET = findViewById(R.id.secondName);
        firstNameET = findViewById(R.id.firstName);
        patronusET = findViewById(R.id.patronus);
        innET = findViewById(R.id.inn);
        sexRG = findViewById(R.id.sexRadioGroup);
        birthDateET = findViewById(R.id.birthDate);
        birthPlaceET = findViewById(R.id.birthPlace);
        citizenshipRG = findViewById(R.id.citizenshipRadioGroup);

    }

    public void onSendButtonClick(View view) {
        String data = "0";
        data += "#";
        data += secondNameET.getText().toString();
        data += "#";
        data += firstNameET.getText().toString();
        data += "#";
        data += patronusET.getText().toString();
        data += "#";
        data += innET.getText().toString();
        data += "#";

        if (sexRG.getCheckedRadioButtonId() == R.id.male) {
            data += "М";
        } else {
            data += "Ж";
        }

        data += "#";
        data += birthDateET.getText().toString();
        data += "#";
        data += birthPlaceET.getText().toString();
        data += "#";

        if (citizenshipRG.getCheckedRadioButtonId() == R.id.rf) {
            data += "Российская Федерация";
        } else if (citizenshipRG.getCheckedRadioButtonId() == R.id.foreign) {
            data += "Иностранный гражданин";
        } else {
            data += "Лицо без гражданства";
        }

        data = data.replaceAll("И", "и");

        sendSMS(data);
    }

    private void sendSMS(String message) {
        sm = SmsManager.getDefault();
        parts = sm.divideMessage(message);

        confirmationDialog.setMessage(  "Ваше заявление отправится с помощью " + parts.size() +
                                        " SMS. Вы уверены, что хотите подать заявление?");
        confirmationDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sm.sendMultipartTextMessage(getString(R.string.server_phone), null, parts, null, null);
                Toast.makeText(IpActivity.this, "Ваше заявление подано.", Toast.LENGTH_LONG).show();
            }
        });

        confirmationDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        confirmationDialog.show();

    }
}
