package com.vava.offline;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class ZagranActivity extends AppCompatActivity {

    EditText    secondNameET, firstNameET, patronusET,
                birthDateET, birthPlaceET, subjectET,
                districtET, townET, streetET,
                houseET, flatET, regDateET;
    RadioGroup sexRG;

    AlertDialog.Builder confirmationDialog;
    SmsManager sm;
    ArrayList<String> parts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zagran);

        confirmationDialog = new AlertDialog.Builder(this);

        secondNameET = findViewById(R.id.secondName);
        firstNameET = findViewById(R.id.firstName);
        patronusET = findViewById(R.id.patronus);
        sexRG = findViewById(R.id.sexRadioGroup);
        birthDateET = findViewById(R.id.birthDate);
        birthPlaceET = findViewById(R.id.birthPlace);
        subjectET = findViewById(R.id.subject);
        districtET = findViewById(R.id.district);
        townET = findViewById(R.id.town);
        streetET = findViewById(R.id.street);
        houseET = findViewById(R.id.house);
        flatET = findViewById(R.id.flat);
        regDateET = findViewById(R.id.regDate);


    }

    public void onSendButtonClick(View view) {

        String data = "1";
        data += "#";
        data += secondNameET.getText().toString();
        data += "#";
        data += firstNameET.getText().toString();
        data += "#";
        data += patronusET.getText().toString();
        data += "#";

        if (sexRG.getCheckedRadioButtonId() == R.id.male) {
            data += "М";
        } else {
            data += "Ж";
        }

        data += "#";
        data += birthDateET.getText().toString();
        data += "#";
        data += birthPlaceET.getText().toString();
        data += "#";
        data += subjectET.getText().toString();
        data += "#";
        data += districtET.getText().toString();
        data += "#";
        data += townET.getText().toString();
        data += "#";
        data += streetET.getText().toString();
        data += "#";
        data += houseET.getText().toString();
        data += "#";
        data += flatET.getText().toString();
        data += "#";

        if (regDateET.getText().toString().equals(""))
            data += " ";
        else
            data += regDateET.getText().toString();

        data = data.replaceAll("И", "и");

        sendSMS(data);
    }

    private void sendSMS(String message) {
        sm = SmsManager.getDefault();
        parts = sm.divideMessage(message);

        confirmationDialog.setMessage(  "Ваше заявление отправится с помощью " + parts.size() +
                " SMS. Вы уверены, что хотите подать заявление?");
        confirmationDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sm.sendMultipartTextMessage(getString(R.string.server_phone), null, parts, null, null);
                Toast.makeText(ZagranActivity.this, "Ваше заявление подано.", Toast.LENGTH_LONG).show();
            }
        });

        confirmationDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        confirmationDialog.show();
    }
}
